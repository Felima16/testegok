//
//  Product.swift
//  testeGOK
//
//  Created by Fernanda  de Lima on 28/11/20.
//

import Foundation

struct HeaderProducts: Codable {
    var spotlight: [Products] = []
    var products: [Products] = []
    var cash: Products = Products()
}

struct Products: Codable {
    var bannerURL: String?
    var imageURL: String?
}
