//
//  ImageCell.swift
//  testeGOK
//
//  Created by Fernanda  de Lima on 28/11/20.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        imageView.layer.cornerRadius = 10
    }
    
    func setupCell(product: Products) {
        if let string = product.bannerURL,
           let url = URL(string: string) {
            downloadImage(from: url)
            return
        }
        
        if let string = product.imageURL,
           let url = URL(string: string) {
            downloadImage(from: url)
            return
        }
    }
    
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
            URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
        }
        
    private func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                self.imageView.image = UIImage(data: data)
            }
        }
    }
    
}
