//
//  ViewController.swift
//  testeGOK
//
//  Created by Fernanda  de Lima on 28/11/20.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var spotlightCV: UICollectionView!
    @IBOutlet weak var productCV: UICollectionView!
    @IBOutlet weak var bannerImage: UIImageView!
    private let reuseIdentifier = "imageCell"
    private let sectionInsets = UIEdgeInsets(top: 20.0,
                                             left: 10.0,
                                             bottom: 20.0,
                                             right: 10.0)
    
    var products: HeaderProducts = HeaderProducts()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        getProducts()        
    }

    override func viewDidLayoutSubviews() {
        bannerImage.layer.cornerRadius = 10
    }
    
    func getProducts() {
        API.get(HeaderProducts.self, endpoint: Endpoint.products) { (result) in
            self.products = result
            self.loadImage()
        } fail: { (error) in
            print("===> error \(error.localizedDescription)")
        }
    }
    
    func loadImage() {
        DispatchQueue.main.async() {
            self.spotlightCV.reloadData()
            self.productCV.reloadData()
        }
        
        if let url = URL(string: products.cash.bannerURL ?? "") {
            downloadImage(from: url)
        }
    }
    
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
            URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
        }
        
    private func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                self.bannerImage.image = UIImage(data: data)
            }
        }
    }
}

extension ViewController: UICollectionViewDelegate {
    
}

extension ViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
      
    func collectionView(_ collectionView: UICollectionView,
                               numberOfItemsInSection section: Int) -> Int {
        if collectionView == spotlightCV {
            return products.spotlight.count
        } else {
            return products.products.count
        }
    }
      
    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
      ) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ImageCell
        // Configure the cell
        if collectionView == spotlightCV {
            cell.setupCell(product: products.spotlight[indexPath.row])
        } else {
            cell.setupCell(product: products.products[indexPath.row])
        }
        return cell
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                          layout collectionViewLayout: UICollectionViewLayout,
                          sizeForItemAt indexPath: IndexPath) -> CGSize {
        var widthItem: CGFloat = 0
        let heightItem: CGFloat = collectionView.frame.height * 0.9
        
        if collectionView == spotlightCV {
            widthItem = heightItem * 2
        } else {
            widthItem = heightItem
        }

        return CGSize(width: widthItem, height: heightItem)
    }
      
    func collectionView(_ collectionView: UICollectionView,
                          layout collectionViewLayout: UICollectionViewLayout,
                          insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}
